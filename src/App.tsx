import { Routes, Route } from "react-router-dom"

import logo from './logo.svg';
import './App.css';

import Navigation from "./components/Navigation/navigation";
import HomeComponent from './components/Home/home';
import AnimeList from './components/List/anime-list';
import Search from './components/Search/anime-search';
import Login from './components/Login/login';
import PageNotFound from "./components/PageNotFound/page-not-found";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Navigation />
      </header>

      <div className='main'>
          <Routes>
            <Route path='/' element={<Login/>}/>
            <Route path='/home' element={<HomeComponent/>}/>
            <Route path='/list' element={<AnimeList/>}/>
            <Route path='/search' element={<Search/>}/>
            <Route path='*' element={<PageNotFound/>}/>
          </Routes>
        </div>
    </div>
  );
}

export default App;
