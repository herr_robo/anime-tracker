import Typography from '@mui/material/Typography';

const getLocation = () => {
    let path = '';

    switch (window.location.pathname) {
        case '/home':
            path = 'Home';
            break;
        case '/list':
            path = 'List';
            break;
        case '/search':
            path = 'Search'
            break;
        default :
            break;
    }

    return path;
}

function PageTitle() {
    return (
        <Typography variant='h3' align='center' sx={{color: 'white'}}>
            Anime {getLocation()} Page
        </Typography>
    )
}

export default PageTitle;