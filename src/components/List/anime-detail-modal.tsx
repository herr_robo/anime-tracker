import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Fade from '@mui/material/Fade';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import EditIcon from '@mui/icons-material/Edit';

import { Services } from '../Services/services';
import { updateAnimeWatchStatusInStateList } from '../animeSlice';
import AnimeEpisodesWatchedField from './anime-episodes-watched-field';
import AnimeWatchStatusSelect from './anime-watch-status-select';
import './anime-detail-modal.css';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    maxWidth: '40%',
    //minWidth: 600,
    height: '70%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    overflow: 'scroll'
};

function AnimeDetailModal(props : any) {
    const [open, setOpen] = useState(false);
    const [edit, setEdit] = useState(false);
    const [episodes, setEpisodes] = useState<number>(props.anime.episodes_watched);
    const [watchStatus, setWatchStatus] = useState(props.anime.watch_status);
    const originalWatched = props.anime.episodes_watched;
    const dispatch = useDispatch();

    const handleClose = () => {
        setEdit(false);
        setOpen(false);
        // if no updates were sent to backend, these revert edits on close
        setEpisodes(props.anime.episodes_watched);
        setWatchStatus(props.anime.watch_status);
        props.onClose();
    }

    useEffect(() => {
        setOpen(props.show);

    }, [props.show]);

    const handleWatchSubmit = (event: any) => {
        event.preventDefault();
        let id = props.anime.anime_id;
        let episodes = event.target[0].value;
        let watchStatus = event.target[1].value;

        if (watchStatus === 'Completed') {
            episodes = props.anime.episodes;
        }

        let body = {
            "anime_id": id,
            "watch_status": watchStatus,
            "episodes_watched": episodes
        };

        Services.updateAnimeWatchProgress(body)
        .then(data => {
            dispatch(updateAnimeWatchStatusInStateList(data));
        })
        .catch(error => {
            console.log(error.message);
            console.warn('Anime Status was not updated');
        });

        setEdit(false);
    }

    const handleEditClick = () => {
        edit ? setEdit(false) : setEdit(true);
    }

    const handleEpisodeChange = (input : any) => setEpisodes(input);

    const handleWatchStatusChange = (input : any) => setWatchStatus(input);

    return (
        <div>
            <Modal open={open} onClose={handleClose} BackdropComponent={Backdrop}>
                <Fade in={open}>
                    <Box sx={style}>
                        <Grid container spacing={2}>
                            <Grid item xs={7}>
                                <Typography id="transition-modal-title" variant="h4" component="h3" paragraph>
                                    {props.anime.title}
                                </Typography>
                                <Typography paragraph>
                                    Duration: {Services.dateConversion(props.anime.start_date)}
                                    {
                                        props.anime.end_date ? <span> - {Services.dateConversion(props.anime.end_date)}</span> : null
                                    }                          
                                </Typography>
                                <Typography paragraph>
                                    Type: {props.anime.anime_type} | Rating: {props.anime.rated}
                                </Typography>
                                <Typography paragraph>
                                    Status: {props.anime.show_status}
                                </Typography>
                                <Typography paragraph>
                                    Episode length: {props.anime.duration}
                                </Typography>
                            </Grid> 
                            <Grid item xs={5}>
                                <img className="image-section"
                                    src={`${props.anime.image_url}`} 
                                    width='100%'
                                    alt={props.anime.title}
                                />
                            </Grid>
                        </Grid>
                            <Divider sx={{width: '50%', marginBottom: '10px'}} />
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Typography id="transition-modal-title" variant="h6" paragraph>
                                    Update your watch progress
                                    <IconButton aria-label="edit anime" onClick={handleEditClick}>
                                        <EditIcon/>
                                    </IconButton>
                                </Typography>
                                <form onSubmit={handleWatchSubmit}>
                                    <div className='status-section'>
                                        Episodes Watched:
                                        {
                                            edit ?  <AnimeEpisodesWatchedField onEpisodeChange={handleEpisodeChange} 
                                                        originalEpisodes={originalWatched} 
                                                        episodesWatched={episodes} 
                                                        maxEpisodes={props.anime.episodes}
                                                    />
                                            :
                                            <span style={{marginLeft: '5px'}}>{props.anime.episodes_watched}</span>
                                        }
                                        <span className='status-span'>of</span> {props.anime.episodes}
                                    </div>
                                    <div className='status-section'>
                                        Watch Status: 
                                        {
                                            edit ?  <AnimeWatchStatusSelect onWatchStatusChange={handleWatchStatusChange}
                                                        watchStatus={watchStatus} 
                                                    />
                                            :
                                            <span className='status-span'>{props.anime.watch_status}</span>
                                        }
                                    </div>
                                    {
                                        edit ? <Button type='submit' variant="contained">Update</Button> : null
                                    }
                                </form>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Typography id="transition-modal-description" sx={{ mt: 3 }}>
                                    {props.anime.synopsis}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>
                </Fade>
            </Modal>
        </div>
    )
}

export default AnimeDetailModal;