
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useState } from 'react';

function AnimeWatchStatusSelect(props : any) {
    const [watchStatus, setWatchStatus] = useState<string>(props.watchStatus);
    const statusList = [
        'Plan to Watch', 'Watching', 'Completed', 'On Hold', 'Dropped'
    ];

    const handleWatchStatusChange = (event : SelectChangeEvent) => {
        let status = event.target.value;

        if (status && (status !== '')) {
            setWatchStatus(status);
            props.onWatchStatusChange(status);
        } else {
            status = 'Plan to Watch';
            setWatchStatus(status);
            props.onWatchStatusChange(status);
        }
    }

    return (
        <FormControl variant="standard">
            <Select style={{marginLeft: '5px'}} value={props.watchStatus}
                onChange={handleWatchStatusChange}
            >
                {statusList.map((status, key) => (
                    <MenuItem value={status} key={key}>{status}</MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}

export default AnimeWatchStatusSelect;