import { useState } from 'react';
import TextField from '@mui/material/TextField';

function AnimeEpisodesWatchedField(props : any) {

    const [episodes, setEpisodes] = useState<number>(props.episodesWatched)
    const originalEpisodeNumber = props.originalEpisodes;
    const episodeChange = (event: any) => {
        let inputEpisodes;
        let max = props.maxEpisodes;

        if (!isNaN(event.target.value) && (event.target.value !== '')) {
            inputEpisodes = parseInt(event.target.value, 10);

            if (inputEpisodes > max) inputEpisodes = max;
            if (inputEpisodes < 0) inputEpisodes = 0;

        } else { // user attempts input clear, so reset to initial value from the parent state
            inputEpisodes = originalEpisodeNumber;
        }
        setEpisodes(inputEpisodes);
        props.onEpisodeChange(inputEpisodes);
    }

    return (
        <TextField sx={{width: '50px', marginLeft: '5px'}} 
            id="outlined-number" 
            size="small" 
            type="number" 
            variant="standard"
            value={props.episodesWatched}
            inputProps={{ min: 0, max: props.maxEpisodes }}
            onChange={episodeChange}
        />
    )
}

export default AnimeEpisodesWatchedField;