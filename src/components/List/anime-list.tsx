import { useSelector } from 'react-redux';

import { selectAnime } from '../animeSlice';
import AnimeCard from './anime-card';
import PageTitle from '../shared/page-title';
import './anime-list.css';

function AnimeList() {
    
    const animeStateList = useSelector(selectAnime);
    
    return (
        <div>
            <PageTitle />
            <p className="list-paragraph">
                Below is the list of anime you have watched. Click on the heart icon to add to your favorites!
            </p>
            <div className="list-section">
                {
                    animeStateList.map((anime : any, index: any) => (
                        <AnimeCard anime={anime} key={index}/>
                    ))
                }
            </div>
        </div>
    )
}

export default AnimeList;