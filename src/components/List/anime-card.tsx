import { useState } from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import RemoveCircleOutlineOutlinedIcon from '@mui/icons-material/RemoveCircleOutlineOutlined';
import EditIcon from '@mui/icons-material/Edit';
import Tooltip from '@mui/material/Tooltip';
import { useDispatch } from 'react-redux';

import { Services } from '../Services/services';
import { removeFromStateList, updateAnimeFavorite } from '../animeSlice';
import AnimeDetailModal from './anime-detail-modal';

const cardStyle = {
	margin: '20px'
}
  
function AnimeCard(props : any) {
	const anime = props.anime;
	const [editAnime, setEditAnime] = useState(false);
	const [favorite, setFavorite] = useState(anime.favorite);
	const dispatch = useDispatch();

	const handleFavoriteClick = () => {
		let favorite = (anime.favorite === 0) ? 1 : 0;
		let body = {
			anime_id: anime.anime_id,
			favorite: favorite
		}

		Services.updateAnimeFavorite(body)
			.then(result => {
				dispatch(updateAnimeFavorite({anime_id: body.anime_id, favorite: favorite}));
				setFavorite(favorite);				
			})
			.catch(error => {
				console.log(error.message);
				console.warn('Anime favorite was not updated');
			})
	}

	const handleRemoveClick = () => {
		Services.removeFromPersonalList(anime.anime_id)
			.then(result => {
				(result.affectedRows === 1) 
					? dispatch(removeFromStateList(result.anime_id)) 
					: console.warn('Anime not removed');
			})
			.catch(error => {
				console.warn(error);
			});
	}

	const handleEditClick = () => setEditAnime(true);

	const onClose = () => setEditAnime(false);

    return (
        <div style={cardStyle}>
            <Card sx={{ width: 300, textAlign: 'left' }}>
                <CardHeader
                    title={anime.title}
                />
                <CardMedia
                    component="img"
                    image={anime.image_url}
                    alt={anime.title}
                    sx={{ maxHeight: 400 }}
                />
                <CardContent>
					<Typography paragraph>
						Duration: {Services.dateConversion(anime.start_date)}
						{
                            anime.end_date ? <span> - {Services.dateConversion(anime.end_date)}</span> : null
                        }   
					</Typography>
					<Typography paragraph>
						Type: {anime.anime_type}
					</Typography>
					<Typography paragraph>
						Rating: {anime.rated}
					</Typography>
					<Typography paragraph>
						Episode length: {anime.duration}
					</Typography>
					<Typography paragraph>
						Status: {anime.show_status}
					</Typography>
					<Typography paragraph>
						Episodes Watched: {anime.episodes_watched} of {anime.episodes}
					</Typography>
                </CardContent>
                <CardActions disableSpacing>
				<Tooltip title="Add to Favorites" arrow>
					<IconButton aria-label="add to favorites" onClick={handleFavoriteClick}>
						<FavoriteIcon htmlColor={favorite ? "red" : undefined}/>
					</IconButton>
				</Tooltip>
				<Tooltip title="Edit Anime" arrow>
					<IconButton aria-label="edit anime" onClick={handleEditClick}>
						<EditIcon/>
					</IconButton>
				</Tooltip>
				<Tooltip title="Remove from List" arrow>
					<IconButton aria-label="remove from personal list" onClick={handleRemoveClick}>
						<RemoveCircleOutlineOutlinedIcon/>
					</IconButton>
				</Tooltip>
                </CardActions>
            </Card>
			
			<AnimeDetailModal onClose={onClose} show={editAnime} anime={anime}/>
        </div>
    )
}

export default AnimeCard;
