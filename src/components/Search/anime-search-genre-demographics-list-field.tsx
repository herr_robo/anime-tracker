import { useState } from 'react';
import FormControl from '@mui/material/FormGroup';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function AnimeSearchGenreDemographicsListField() {

    const [selectedDemographics, setSelectedDemographics] = useState<any[]>([]);

    const genreDemographicsList = [
        { "mal_id": 43, "name": "Josei" },
        { "mal_id": 15, "name": "Kids" },
        { "mal_id": 42, "name": "Seinen" },
        { "mal_id": 25, "name": "Shoujo" },
        { "mal_id": 27, "name": "Shounen" }
    ];

    const handleDemographicChange = (event: SelectChangeEvent<typeof selectedDemographics>) => {

        const { 
            target: { value },
        } = event;

        setSelectedDemographics(
            typeof value === 'string' ? value.split(',') : value,
        );
    };

    const renderResults = (input: any[]) => {
        let nameList;

        if (input.length === 0) {
            return;
        }

        nameList = genreDemographicsList.filter((item) => {
            if(input.includes(item.mal_id)) {
                return item.name;
            }
        }).map(name => name.name);

        return nameList.join(', ');
    }

    return (
        <FormControl className='search-field-input'>
            <InputLabel id="demographic-selection">Demographics</InputLabel>
            <Select
                onChange={handleDemographicChange} 
                value={selectedDemographics}
                multiple
                MenuProps={MenuProps}
                labelId="demographic-selection"
                input={<OutlinedInput label="Themes"/>}
                renderValue={renderResults}
            >
            {
                genreDemographicsList.map((demographic) => (
                    <MenuItem value={demographic.mal_id} key={demographic.mal_id}>
                        <Checkbox checked={selectedDemographics.indexOf(demographic.mal_id) > -1}/>
                        <ListItemText primary={demographic.name}/>
                    </MenuItem>
                ))
            }
            </Select>
        </FormControl>
    )
}

export default AnimeSearchGenreDemographicsListField;