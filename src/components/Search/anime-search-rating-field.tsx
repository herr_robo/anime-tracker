import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useState } from 'react';

function AnimeSearchRatingField() {

    const [animeRating, setAnimeRating] = useState('');

    const ratingList = [
        { name: 'G - All Ages', value: 'g' },
        { name: 'PG - Children', value: 'pg' },
        { name: 'PG-13 - Teens 13 or older', value: 'pg13' },
        { name: 'R - 17+ (violence & profanity)', value: 'r17' },
        { name: 'R+ - Mild Nudity', value: 'r' },
        { name: 'Rx - Hentai', value: 'rx' }
    ]

    const handleRatingChange = (event : SelectChangeEvent) => {
        let rating = event.target.value;
        setAnimeRating(rating);
    }

    return (
        <FormControl
            sx={{ marginBottom: '20px'}}
            className='search-field-input'
        >
            <InputLabel>Rating</InputLabel>
            <Select onChange={handleRatingChange} value={animeRating} label="Rating">
                {
                    ratingList.map((rating, index) => (
                        <MenuItem value={rating.value} key={index}>{rating.name}</MenuItem>
                    ))
                }
            </Select>
        </FormControl>
    )
}

export default AnimeSearchRatingField;