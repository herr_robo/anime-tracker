import { useState } from 'react';
import FormControl from '@mui/material/FormGroup';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function AnimeSearchGenreExplicitListField() {

    const [selectedExplicitGenres, setSelectedExplicitGenres] = useState<any[]>([]);
    const explicitGenreList = [
        { "mal_id": 9, "name": "Ecchi" },
        { "mal_id": 49, "name": "Erotica" },
        { "mal_id": 12, "name": "Hentai" }
    ];

    const handleExplicitGenreChange = (event: SelectChangeEvent<typeof selectedExplicitGenres>) => {

        const { 
            target: { value },
        } = event;

        setSelectedExplicitGenres(
            typeof value === 'string' ? value.split(',') : value,
        );
    };

    const renderResults = (input: any[]) => {
        let nameList;

        if (input.length === 0) {
            return;
        }

        nameList = explicitGenreList.filter((item) => {
            if(input.includes(item.mal_id)) {
                return item.name;
            }
        }).map(name => name.name);

        return nameList.join(', ');
    }

    return (
        <FormControl className='search-field-input'>
            <InputLabel id="explicit-genre-selection">Explicit Genres</InputLabel>
            <Select
                onChange={handleExplicitGenreChange} 
                value={selectedExplicitGenres}
                multiple
                MenuProps={MenuProps}
                labelId="explicit-genre-selection"
                input={<OutlinedInput label="Themes"/>}
                renderValue={renderResults}
            >
            {
                explicitGenreList.map((genre) => (
                    <MenuItem value={genre.mal_id} key={genre.mal_id}>
                        <Checkbox checked={selectedExplicitGenres.indexOf(genre.mal_id) > -1}/>
                        <ListItemText primary={genre.name}/>
                    </MenuItem>
                ))
            }
            </Select>
        </FormControl>
    )
}

export default AnimeSearchGenreExplicitListField;