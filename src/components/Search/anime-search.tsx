import { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel'
import TextField from '@mui/material/TextField';
import { DataGrid, GridColDef, GridSelectionModel, GridRowParams } from '@mui/x-data-grid';
import { useDispatch, useSelector } from 'react-redux';

import { addToStateList, selectAnime } from '../animeSlice';
import { Services } from '../Services/services'
import AnimeSearchTypeField from './anime-search-type-field';
import AnimeSearchRatingField from './anime-search-rating-field';
import AnimeSearchGenreListField from './anime-search-genre-list-field';
import AnimeSearchGenreThemesListField from './anime-search-genre-themes-list-field';
import AnimeSearchGenreExplicitListField from './anime-search-genre-explicit-list-field';
import AnimeSearchGenreDemographicsListField from './anime-search-genre-demographics-list-field';

import PageTitle from '../shared/page-title';
import './anime-search.css';

function AnimeSearch() {

    const [searchList, setSearchList] = useState([]);
    const [selectionModel, setSelectionModel] = useState<GridSelectionModel>([]);
    const [disabledAdd, setDisabledAdd] = useState(true);
    const [disabledSearch, setDisabledSearch] = useState(false);
    const [showExplicitField, setShowExplicitField] = useState(false);
    const animeList = useSelector(selectAnime);

    const dispatch = useDispatch();

    const handleSubmit = (e : any) => {
        setDisabledSearch(true);
        setTimeout(() => {setDisabledSearch(false);}, 3000);
        e.preventDefault();

        let title = e.target[0].value;
        let type = e.target[2].value;
        let rating = e.target[4].value;
        let genres = e.target[6].value;
        let themes = e.target[8].value;
        let demographics = e.target[10].value;
        let explicit = e.target[12].value;
        let genreSearch = '';

        if (genres) {
            genreSearch += genres;
        }

        if (themes) {
            genreSearch ? genreSearch = genreSearch + ',' + themes : genreSearch += themes;
        }

        if (demographics) {
            genreSearch ? genreSearch = genreSearch + ',' + demographics : genreSearch += demographics;
        }

        if (explicit) {
            genreSearch ? genreSearch = genreSearch + ',' + explicit : genreSearch += explicit;
        }

        getAnimeByNameJikan(title, type, rating, genreSearch);
    }

    const handleAddClick = () => {
        selectionModel.length > 1 ? addMultipleAnime(selectionModel) : addSingleAnime(selectionModel);
    }

    const handleShowExplicitClick = () => {
        showExplicitField ? setShowExplicitField(false) : setShowExplicitField(true);
    }

    const addSingleAnime = (id : any[]) => {
        let single : any[] = searchList.filter((anime : any) => id[0] === anime.id);
        single[0].watch_status = 'Plan to Watch';
        single[0].favorite = 0;
        delete single[0].id;
        Services.addToPersonalList(single[0])
            .then(anime => {
                dispatch(addToStateList(anime));
            })
            .catch(error => {
                console.warn(error);
            })
    }

    const addMultipleAnime = (idList : any[]) => {
        console.log('addMultipleAnime', idList);
    }

    const getAnimeByNameJikan = (title : any, type: any, rating: any, genreSearch: any) => {

        let url = 'https://api.jikan.moe/v4/anime?q=' + title;

        if (type) {
            url += '&type=' + type;
        }

        if (rating) {
            url += '&rating=' + rating;
        }

        if (genreSearch) {
            url += '&genres=' + genreSearch;
        }

        fetch(url)
        .then(response => response.json())
        .then(list => {
            console.log('list', list);
            setSearchList(list.data.map((anime : any) => ({
                id: anime.mal_id,
                mal_id: anime.mal_id,
                title: anime.title,
                url: anime.url,
                image_url: anime.images.jpg.image_url,
                synopsis: anime.synopsis,
                show_status: anime.status,
                episodes: anime.episodes,
                anime_type: anime.type,
                rated: anime.rating,
                airing: anime.airing,
                score: anime.score,
                start_date: anime.aired.from,
                end_date: anime.aired.to,
                episodes_watched: 0,
                watch_status: null,
                duration: anime.duration,
                favorite: 0
            //    genres: anime.genres, //reminder for later
            //    themes: anime.themes //reminder for later
            })))
        })
        .catch(error => console.error(error));
    }

    const isCheckable = (id : any ) => {
        let isFound;
        let index = animeList.findIndex((anime : any) => {
           if (anime.mal_id === id) {
               return true;
           }
        });

        isFound = (index === -1) ? true : false;
        return isFound;
    }

    const columns: GridColDef[] = [
        { 
            field: 'id', 
            headerName: 'MAL Id', 
            width: 80
        },
        {
            field: 'title',
            headerName: 'Title',
            width: 450,
        },
        {
            field: 'show_status',
            headerName: 'Status',
            width: 150,
        },
        {
            field: 'episodes',
            headerName: 'Episodes',
            width: 90,
        },
        {
            field: 'anime_type',
            headerName: 'Type',
            width: 100,
        },
        {
            field: 'rated',
            headerName: 'Rated',
            width: 150,
        },
    ];

    useEffect(() => {
        console.log('useEffect');
        selectionModel.length > 0 ? setDisabledAdd(false) : setDisabledAdd(true);
    }, [selectionModel.length]);

    return (
        <div>
            <PageTitle />
            <div className="search-container">
                <div className="search-container-inner">
                    <div className="search-fields-container">
                        <form onSubmit={handleSubmit}>
                            <div className="search-field-row">
                                <TextField 
                                    id="outlined-search" 
                                    label="Search field" 
                                    type="search"
                                    required
                                    sx={{width: '100%'}}
                                />
                            </div>
                            <div className="search-field-row second">
                                <AnimeSearchTypeField />
                                <AnimeSearchRatingField />
                            </div>
                            <div className="search-field-row">
                                <AnimeSearchGenreListField />
                                <AnimeSearchGenreThemesListField />
                            </div>
                            <div className="search-field-row">
                                <AnimeSearchGenreDemographicsListField />
                                {
                                    showExplicitField ? <AnimeSearchGenreExplicitListField />
                                    : null
                                }
                            </div>
                            <div className="search-field-row last">
                                <Button 
                                    variant="contained" 
                                    type="submit" 
                                    disabled={disabledSearch}
                                    sx={{marginTop: '10px'}}>
                                        Search
                                </Button>
                                <FormControlLabel
                                    sx={{marginLeft: '20px', marginTop: '10px'}}                    
                                    value="start"
                                    control={   
                                        <Checkbox
                                            checked={showExplicitField}
                                            onChange={handleShowExplicitClick}
                                            inputProps={{ 'aria-label': 'controlled' }}
                                        />}
                                    label="Show Explicit Field"
                                    labelPlacement="start"
                                />
                            </div>
                        </form>
                    </div>
                    {
                        searchList.length > 0 ? 
                            <div style={{ height: 400}}>
                                <DataGrid
                                    rows={searchList}
                                    columns={columns}
                                    checkboxSelection
                                    onSelectionModelChange={(newSelectionModel) => {
                                        setSelectionModel(newSelectionModel);
                                    }}
                                    selectionModel={selectionModel}
                                    pageSize={25}
                                    isRowSelectable={(params: GridRowParams) => isCheckable(params.row.id) }
                                />
                            </div> : null
                    }
                    <div>
                        <Button 
                            variant="contained" 
                            sx={{ marginTop: '10px' }}
                            disabled={disabledAdd}
                            onClick={handleAddClick}
                        >
                            Add Anime
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AnimeSearch;