import { useState } from 'react';
import FormControl from '@mui/material/FormGroup';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function AnimeSearchGenreListField() {

    const [selectedGenreList, setSelectedGenreList] = useState<any[]>([]);

    const genreList = [
        { "mal_id": 1, "name": "Action" },
        { "mal_id": 2, "name": "Adventure" },
        { "mal_id": 5, "name": "Avant Garde" },
        { "mal_id": 46, "name": "Award Winning" },
        { "mal_id": 28, "name": "Boys Love" },
        { "mal_id": 4, "name": "Comedy" },
        { "mal_id": 8, "name": "Drama" },
        { "mal_id": 10, "name": "Fantasy" },
        { "mal_id": 26, "name": "Girls Love" },
        { "mal_id": 47, "name": "Gourmet" },
        { "mal_id": 14, "name": "Horror" },
        { "mal_id": 7, "name": "Mystery" },
        { "mal_id": 22, "name": "Romance" },
        { "mal_id": 24, "name": "Sci-Fi" },
        { "mal_id": 36, "name": "Slice of Life" },
        { "mal_id": 30, "name": "Sports" },
        { "mal_id": 37, "name": "Supernatural" },
        { "mal_id": 41, "name": "Suspense" },
        { "mal_id": 48, "name": "Work Life" }
    ];

    const handleGenreChange = (event: SelectChangeEvent<typeof selectedGenreList>) => {

        const {
            target: { value },
        } = event;

        setSelectedGenreList(
            typeof value === 'string' ? value.split(',') : value,
        );
    };

    const renderResults = (input : any[]) => {
        let nameList;

        if (input.length === 0) {
            return;
        }

        nameList = genreList.filter((item) => {
            if (input.includes(item.mal_id)) {
                return item.name;
            }
        }).map(name => name.name);

        return  nameList.join(', ');
    }

    return (
        <FormControl className='search-field-input'>
            <InputLabel id="genre-selection">Genres</InputLabel>
            <Select 
                labelId="genre-selection"
                id="genre-selection-checkbox"
                multiple 
                value={selectedGenreList}
                onChange={handleGenreChange} 
                input={<OutlinedInput label="Genres" />}
                renderValue={renderResults}
                MenuProps={MenuProps}
            >
            {
                genreList.map((genre) => (
                    <MenuItem  value={genre.mal_id} key={genre.mal_id}>
                        <Checkbox checked={selectedGenreList.indexOf(genre.mal_id) > -1} />
                        <ListItemText primary={genre.name} />
                    </MenuItem>
                ))
            }
            </Select>
        </FormControl>
    )
}

export default AnimeSearchGenreListField;