import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useState } from 'react';

function AnimeSearchTypeField() {

    const [animeType, setAnimeType] = useState('');

    const typeList = [
        { name: 'TV', value: 'tv' },
        { name: 'Movie', value: 'movie' }, 
        { name: 'OVA', value: 'ova' },
        { name: 'Special', value: 'special' },
        { name: 'ONA', value: 'ona' },
        { name: 'Music', value: 'music' }
    ];

    const handleTypeChange = (event: SelectChangeEvent) => {
        let type = event.target.value;
        setAnimeType(type);
    }

    return (
        <FormControl 
            sx={{ marginBottom: '20px'}}
            className='search-field-input'
        >
            <InputLabel>Type</InputLabel>
            <Select onChange={handleTypeChange} value={animeType} label="Type">
                {
                    typeList.map((type, index) => (
                        <MenuItem value={type.value} key={index}>{type.name}</MenuItem>
                    ))
                }
            </Select>
        </FormControl>
    )
}

export default AnimeSearchTypeField;