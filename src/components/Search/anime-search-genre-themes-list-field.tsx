import { useState } from 'react';
import FormControl from '@mui/material/FormGroup';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function AnimeSearchGenreThemesListField() {

    const [selectedThemes, setSelectedThemes] = useState<any[]>([]);

    const genreThemesList = [
        { "mal_id": 3, "name": "Cars" },
        { "mal_id": 6, "name": "Demons" },
        { "mal_id": 11, "name": "Game" },
        { "mal_id": 35, "name": "Harem" },
        { "mal_id": 13, "name": "Historical" },
        { "mal_id": 17,  "name": "Martial Arts" },
        { "mal_id": 18, "name": "Mecha" },
        { "mal_id": 38, "name": "Military" },
        { "mal_id": 19, "name": "Music" },
        { "mal_id": 20, "name": "Parody" },
        { "mal_id": 39, "name": "Police" },
        { "mal_id": 40, "name": "Psychological" },
        { "mal_id": 21, "name": "Samurai" },
        { "mal_id": 23, "name": "School" },
        { "mal_id": 29, "name": "Space" },
        { "mal_id": 31, "name": "Super Power" },
        { "mal_id": 32, "name": "Vampire" }
    ];

    const handleThemeChange = (event: SelectChangeEvent<typeof selectedThemes>) => {

        const { 
            target: { value },
        } = event;

        setSelectedThemes(
            typeof value === 'string' ? value.split(',') : value,
        );
    };

    const renderResults = (input: any[]) => {
        let nameList;

        if (input.length === 0) {
            return;
        }

        nameList = genreThemesList.filter((item) => {
            if(input.includes(item.mal_id)) {
                return item.name;
            }
        }).map(name => name.name);

        return nameList.join(', ');
    }

    return (
        <FormControl className='search-field-input'>
            <InputLabel id="theme-selection">Themes</InputLabel>
            <Select
                onChange={handleThemeChange} 
                value={selectedThemes}
                multiple
                MenuProps={MenuProps}
                labelId="theme-selection"
                input={<OutlinedInput label="Themes"/>}
                renderValue={renderResults}
            >
            {
                genreThemesList.map((theme) => (
                    <MenuItem value={theme.mal_id} key={theme.mal_id}>
                        <Checkbox checked={selectedThemes.indexOf(theme.mal_id) > -1}/>
                        <ListItemText primary={theme.name}/>
                    </MenuItem>
                ))
            }
            </Select>
        </FormControl>
    )
}

export default AnimeSearchGenreThemesListField;