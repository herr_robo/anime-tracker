import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    anime: [] as any[]
}

export const animeSlice = createSlice({
    name: 'anime',
    initialState,
    reducers: {
        populate: (state, action) => {
            if (action.payload.length > 0) {
                state.anime = action.payload;
            }
        },

        removeFromStateList: (state, action) => {
            const id = parseInt(action.payload);
            console.log('STATE removeFromList id', id);
            state.anime = state.anime.filter((anime : any) => anime.anime_id !== id);
        },

        addToStateList: (state, action) => {
            const singleAnime = action.payload;
            console.log('STATE addToList singleAnime ', singleAnime);
            state.anime.push(singleAnime);
        },

        updateAnimeWatchStatusInStateList: (state, action) => {
            console.log('STATE updateAnimeWatchStatusInStateList', action.payload);
            let id = parseInt(action.payload.id);
            let watch_status = action.payload.watch_status;
            let episodes_watched = parseInt(action.payload.episodes_watched);
            state.anime = state.anime.map((anime : any) => {
                if (anime.anime_id === id) {
                    anime.watch_status = watch_status;
                    anime.episodes_watched = episodes_watched;
                }
                return anime;
            });
        },

        updateAnimeFavorite: (state, action) => {
            console.log('STATE updateAnimeFavorite', action.payload);
            let id = action.payload.anime_id;
            let favorite = action.payload.favorite; 
            state.anime = state.anime.map((anime : any) => {
                if (anime.anime_id === id) {
                    anime.favorite = favorite;
                }
                return anime;
            })

        }
    }
})

export const { populate, removeFromStateList, addToStateList, updateAnimeWatchStatusInStateList, updateAnimeFavorite } = animeSlice.actions;

export const selectAnime = (state : any) => state.anime.anime;

export default animeSlice.reducer;