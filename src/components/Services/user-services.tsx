
const login = async (user: any) => {
    let response = await fetch('http://localhost:5000/api/user/login', {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log('response', response);

    if (!response.ok) {
        const message = `An error occured: ${response.status}`;
        throw new Error(message);
    }

    let data = await response.json();
    return data;
}

export const UserServices = {
    login
};