
    const removeFromPersonalList = async (id : any) => {
      let response = await fetch(`http://localhost:5000/api/anime/${id}`, {
        method: 'DELETE',
        mode: 'cors',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
      });

      if (!response.ok) {
        const message = `An error occured: ${response.status}`;
        throw new Error(message);
      }

      let data = await response.json();
      return data;
    }

    const addToPersonalList = async (anime : any) => {
      let response = await fetch('http://localhost:5000/api/anime/', {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(anime)
      });

      if (!response.ok) {
        const message = `An error has occured: ${response.status}`;
        throw new Error(message);
      }

      let data = await response.json();
      return data.data;
    }

    const updateAnimeWatchProgress = async (body : any) => {
      let response = await fetch(`http://localhost:5000/api/anime/episodes`, {
          method: 'PUT',
          mode: 'cors',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify(body)
      });

      if (!response.ok) {
        const message = `An error has occured: ${response.status}`;
        throw new Error(message);
      }

      let data = await response.json();
      return data;
  }

  const updateAnimeFavorite = async (body : any) => {
      let response = await fetch(`http://localhost:5000/api/anime/favorite`, {
        method: 'PUT',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
      });
  
      if (!response.ok) {
        const message = `An error has occured: ${response.status}`;
        throw new Error(message);
      }

      let data = await response.json();
      return data;
  }

  const getAnimeList = async () => {
    try {
      let response = await fetch('http://localhost:5000/api/anime/', {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
      });

      let data = await response.json(); 
      return data.data;
    }

    catch (err) {
      console.error(err);
      return err;
    }
  }

  const dateConversion = (input : any) => {
    let result = new Date(input).toLocaleString().split(',')[0];
    return result;
  }

export const Services = { 
  removeFromPersonalList, 
  addToPersonalList, 
  updateAnimeWatchProgress, 
  updateAnimeFavorite, 
  getAnimeList, 
  dateConversion 
};