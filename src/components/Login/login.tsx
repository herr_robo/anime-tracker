import { useNavigate } from 'react-router-dom';
import { ChangeEvent, MouseEvent, useState } from 'react';

import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

import { UserServices } from '../Services/user-services';
import './login.css';

interface State {
    username: string,
    password: string,
    showPassword: boolean
}

function Login() {
    const navigate = useNavigate();
    const [loginValues, setLoginValues] = useState<State>({
        username: '',
        password: '',
        showPassword: false
    })
    const [disabled, setDisabled] = useState<boolean>(true);
    const [usernameError, setUsernameError] = useState<boolean>(false);
    const [passwordError, setPasswordError] = useState<boolean>(false);
    const [capslock, setCapslock] = useState<boolean>(false);

    const handleChange = (prop: keyof State) => (event: ChangeEvent<HTMLInputElement>) => {
        if (prop === 'username') {
            ((event.target.value.length > 0) && (loginValues.password.length > 0)) ? 
                setDisabled(false) : setDisabled(true);
        } 
        else if (prop === 'password') { 
            ((event.target.value.length > 0) && (loginValues.username.length > 0)) ? 
                setDisabled(false) : setDisabled(true);
        }

        setLoginValues({...loginValues, [prop]: event.target.value});
    }

    const handleClickShowPassword = () => {
        setLoginValues({
            ...loginValues,
            showPassword: !loginValues.showPassword
        })
    }

    const handleMouseDownPassword = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const handleCapsButton = (event: any) => {
        let capsLockStatus = event.getModifierState('CapsLock');
        setCapslock(capsLockStatus); // OR mEMO?
    }

    const handleLogin = (event : any) => {
        event.preventDefault();
        const user = {
            username: event.target[0].value,
            password: event.target[2].value
        };

        UserServices.login(user)
            .then(thing => {
                navigate('/home');
            })
            .catch(error => {
                console.warn('Error: ', error);
                setUsernameError(true);
                setPasswordError(true);
            })
    }

    return (
        <div>
            <Typography variant='h3' align='center' sx={{color: 'white'}}>
                Welcome to the Anime Tracker
            </Typography>
            <Container>
                <Box sx={{ 
                    bgcolor: '#ffffff', 
                    height: '100vh', 
                    borderRadius: '10px',
                    paddingTop: '20px'
                }} >
                    <form className='login-container' onSubmit={handleLogin} >
                        <FormControl sx={{ m: 2, width: '75%'}} variant='outlined'>
                            <InputLabel htmlFor='username-field'>User Name</InputLabel>
                            <OutlinedInput 
                                id="username-field"
                                type='text'
                                value={loginValues.username}
                                onChange={handleChange('username')}
                                label='User Name'
                                error={usernameError}
                            />
                        </FormControl>
                        <FormControl sx={{ m: 2, width: '75%'}} variant='outlined'>
                            <InputLabel htmlFor='password-field'>Password</InputLabel>
                            <OutlinedInput 
                                id='password-field'
                                type={loginValues.showPassword ? 'text' : 'password'}
                                value={loginValues.password}
                                onChange={handleChange('password')}
                                endAdornment={
                                    <InputAdornment position='end'>
                                        <IconButton
                                            aria-label='toggle password visibility'
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {loginValues.showPassword ? <VisibilityOff/> : <Visibility/>}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                                error={passwordError}
                                onKeyUp={handleCapsButton}
                            />
                            {
                                (usernameError || passwordError)
                                    ? 
                                    <div className='login-error'>Invalid Username or Password</div>
                                    : null
                            }

                            {
                                capslock 
                                    ?   
                                    <Alert severity="error">
                                        <strong>Caps Lock key is on</strong>
                                    </Alert>
                                    : null
                            }
                        </FormControl>
                        <Button sx={{ m: 2, width: '75%' }} size="large" type='submit' variant='contained' disabled={disabled}>Login</Button>
                    </form>
                </Box>
            </Container>
        </div>
    )
}

export default Login;