import { Link } from 'react-router-dom';

function Navigation() {
    return (
        <nav>
            <ul>
                <li><Link to='/home'>Home</Link></li>
                <li><Link to='/list'>Anime List</Link></li>
                <li><Link to='/search'>Anime Search</Link></li>
            </ul>
        </nav>
    );
}

export default Navigation;