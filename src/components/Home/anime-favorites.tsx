import Divider from '@mui/material/Divider';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';

function AnimeFavorites(props : any) {

    const [favorites, setFavorites] = useState([]);
    const makeFavoritesList = (list : any) => {
        console.log('makeFavoritesList list', list);
        let favoriteList = list.filter((anime : any) =>  anime.favorite);
        setFavorites(favoriteList);
    }

    useEffect(() => {
        makeFavoritesList(props.list);
    },[props])

    return (
        <div>
            <Typography variant="h5" gutterBottom component="div">
                Favorites <FavoriteIcon htmlColor="red" sx={{verticalAlign: 'text-top'}}/> {favorites.length} 
            </Typography>
            <Divider/>
            <ImageList sx={{ height: 400 }}>
                {favorites.map((anime : any) => (
                    <ImageListItem key={anime.image_url}>
                        <img 
                            src={`${anime.image_url}?w=248&fit=crop&auto=format`}
                            srcSet={`${anime.image_url}?w=248&fit=crop&auto=format&dpr=2 2x`}
                            alt={anime.title}
                            loading="lazy"
                        />
                        <ImageListItemBar 
                            title={anime.title} 
                            sx={{
                                '& .MuiImageListItemBar-title': {
                                    whiteSpace: 'normal',
                                    textOverflow: 'initial',
                                    overflow: 'initial'
                                }
                            }}/>
                    </ImageListItem>
                ))}
            </ImageList>
        </div>
    )
}
export default AnimeFavorites;