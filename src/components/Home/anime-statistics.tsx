import { useEffect, useState } from 'react';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';

function AnimeStatistics(props : any) {
    const [watching, setWatching] = useState(0);
    const [completed, setCompleted] = useState(0);
    const [onHold, setOnHold] = useState(0);
    const [dropped, setDropped] = useState(0);
    const [planToWatch, setPlanToWatch] = useState(0);
    const [totalEntries, setTotalEntries] = useState(0);
    const [totalEpisodesWatched, setTotalEpisodesWatched] = useState(0);
    const [totalPossibleEpisodes, setTotalPossibleEpisodes] = useState(0);
    const [totalMovies, setTotalMovies] = useState(0);
    const [totalSpecialONAEpisodesWatched, setTotalSpecialONAEpisodesWatched] = useState(0);
    const [totalSpecialONAEpisodesPossible, setTotalSpecialONAEpisodesPossible] = useState(0);
    
    let numWatching : number = 0;
    let numCompleted : number = 0;
    let numOnHold : number = 0; 
    let numDropped : number = 0;
    let numPlanToWatch : number = 0;
    let numTotalEntries : number = 0;
    let numTotalEpisodesWatched : number = 0;
    let numTotalPossibleEpisodes : number = 0;
    let numTotalMovies : number = 0;
    let numTotalSpecialONAEpisodesWatched : number = 0;
    let numtotalSpecialONAEpisodesPossible : number = 0;

    const calculateTotals = (list : any) => {
        numTotalEntries = list.length;
        list.forEach((anime : any) =>  {
            switch (anime.watch_status) {
                case 'Watching':
                    numWatching++;
                    break;
                case 'Completed':
                    numCompleted++;
                    break;
                case 'On Hold':
                    numOnHold++;
                    break;
                case 'Dropped':
                    numDropped++;
                    break;
                case 'Plan to Watch':
                    numPlanToWatch++;
                    break;
                default: 
                    break;           
            }

            switch (anime.anime_type) {
                case 'TV':
                case 'OVA':
                    numTotalEpisodesWatched += parseInt(anime.episodes_watched);
                    numTotalPossibleEpisodes += parseInt(anime.episodes);
                    break;
                case 'Movie':
                    numTotalMovies++;
                    break;
                case 'Special':
                case 'ONA':
                    numTotalSpecialONAEpisodesWatched += parseInt(anime.episodes_watched);
                    numtotalSpecialONAEpisodesPossible += anime.episodes;
                break;
                default: 
                    break;
            }
        });
        setWatching(numWatching);
        setCompleted(numCompleted);
        setOnHold(numOnHold);
        setDropped(numDropped);
        setPlanToWatch(numPlanToWatch);
        setTotalEntries(numTotalEntries);
        setTotalEpisodesWatched(numTotalEpisodesWatched);
        setTotalPossibleEpisodes(numTotalPossibleEpisodes);
        setTotalMovies(numTotalMovies);
        setTotalSpecialONAEpisodesWatched(numTotalSpecialONAEpisodesWatched);
        setTotalSpecialONAEpisodesPossible(numtotalSpecialONAEpisodesPossible);
    }

    useEffect(() => {
        if (props.list.length > 0) {
            calculateTotals(props.list);
        }
    }, [props, calculateTotals]);

    return (
        <div>
            <Typography variant="h5" gutterBottom component="div">
                Watching Statistics
            </Typography>
            
            <Divider />
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <TableContainer>
                        <Table>
                            <TableBody>
                                <TableRow>
                                    <TableCell align="left">Watching</TableCell>
                                    <TableCell align="right">{watching}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell align="left">Completed</TableCell>
                                    <TableCell align="right">{completed}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell align="left">On-Hold</TableCell>
                                    <TableCell align="right">{onHold}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell align="left">Dropped</TableCell>
                                    <TableCell align="right">{dropped}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell 
                                        align="left" 
                                        sx={{borderBottom: {
                                            sm: '0px',
                                            md: '1px'
                                        }}}>
                                            Plan to Watch
                                    </TableCell>
                                    <TableCell 
                                        align="right" 
                                        sx={{borderBottom: {
                                            sm: '0px',
                                            md: '1px'
                                        }}}>{planToWatch}
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TableContainer>
                        <Table>
                            <TableBody>
                                <TableRow>
                                    <TableCell align="left">Total Entries</TableCell>
                                    <TableCell align="right">{totalEntries}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell align="left">
                                        Episodes <br/>
                                        (TV/OVAs)
                                    </TableCell>
                                    <TableCell align="right" sx={{verticalAlign: 'top'}}>
                                        {totalEpisodesWatched} of {totalPossibleEpisodes}
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell align="left">Movies</TableCell>
                                    <TableCell align="right">{totalMovies}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell align="left" sx={{borderBottom: 'none'}}>
                                        Episodes <br/>
                                        (Specials/ONAs)
                                    </TableCell>
                                    <TableCell align="right" sx={{verticalAlign: 'top', borderBottom: 'none'}}>
                                        {totalSpecialONAEpisodesWatched} of {totalSpecialONAEpisodesPossible}
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        </div>
    )
}

export default AnimeStatistics;