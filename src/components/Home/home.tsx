import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';

import { populate, selectAnime } from '../animeSlice';
import { Services } from '../Services/services';
import AnimeStatistics from './anime-statistics';
import AnimeFavorites from './anime-favorites';
import PageTitle from '../shared/page-title';
import './home.css';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));

function HomeComponent() {
    const animeStateList = useSelector(selectAnime);
    const dispatch = useDispatch();

    useEffect(() => {
        console.log('useEffect animeStateList', animeStateList);
        if (animeStateList.length < 1) {
            Services.getAnimeList()
            .then((data : any) => {
                if (data.length > 0) {
                    dispatch(populate(data));
                }
            });
        }
    }, [animeStateList, dispatch]); 

    return (
        <div className="container">
            <PageTitle />
            <Container>
                <Box sx={{ bgcolor: '#ffffff', height: '100vh', borderRadius: '10px' }} >
                    <Grid container spacing={1}> 
                        <Grid item xs={12} sm={12} md={12}>
                            <Item>
                                <AnimeStatistics list={animeStateList} />
                            </Item>
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <Item>
                                <AnimeFavorites list={animeStateList}/>
                            </Item>
                        </Grid>
                    </Grid>
                </Box>
            </Container>
        </div>
    )
}

export default HomeComponent;